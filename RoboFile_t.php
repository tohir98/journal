<?php

use packager\PackagerTasks;
use Robo\Result;
use Robo\Task\SshExecTask;
use Robo\Tasks;

//crappy codeIgniter check
if (!defined('BASEPATH')) {
    define('BASEPATH', __DIR__ . '/system');
}

//crappy codeIgniter check
if (!defined('APP_ROOT')) {
    define('APP_ROOT', __DIR__ . DIRECTORY_SEPARATOR);
}

define('ENVIRONMENT', getenv('TB_ENV') ? : 'development');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            ini_set('display_errors', 'On');
            break;
        case 'testing':
        case 'production':
            ini_set('display_errors', 'Off');
            break;

        default:
            exit('The application environment is not set correctly.');
    }
}

define('FCPATH', __DIR__);
require_once 'dbvc/DBMigration.php';
require_once 'application/config/constants.php';
require_once 'application/libraries/MigrateToS3.php';

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends Tasks {

    protected static $deploymentConfig = array(
        'testing' => array(
            'server' => ['testhired.com.ng'],
        ),
        'production' => array(
            'user' => 'wwwuser',
            'server' => ['portal.talentbase.ng'],
            'branch' => 'release'
        )
    );
    protected static $defaultDeploymentConfig = array(
        'app_path' => '~/public_html',
        'port' => 22,
        'branch' => 'development',
        'user' => 'wwwuser',
        'composer_path' => null,
    );

    // define public methods as commands

    /**
     * Perform database migration, see wiki at https://bitbucket.org/talentbase/talentbasev01/wiki/Database%20Migration%20Guide
     * for details on database migration
     * @param string $dbGroup the group to migrate.
     * @option $start int the start version
     * @option $mode the mode in which to run the migration
     *                  Available modes include interactive|non_interactive|dryrun
     * @return int 0 on success any other value otherwise
     */
    public function dbMigrate($dbGroup = 'default', $opts = ['start' => 340, 'mode' => DBMigrationMode::NON_INTERACTIVE]) {
        $mode = $opts['mode'];
        $startVersion = $opts['start'];

        return include('dbvc/migration.php');
    }

    protected function getDeploymentConfig($env) {
        return array_merge(self::$defaultDeploymentConfig, (array) self::$deploymentConfig[$env]);
    }

    protected function taskDeploy($env, $identityFile = '', array $options) {
        $config = $this->getDeploymentConfig($env);

        if (!$options['skip-push']) {
            $this->say('Auto updating your working tree..');
            $result = $this->taskGitStack()
                    ->add('-A')
                    ->commit('"Autoupdate before deployment"')
                    ->pull('origin', $config['branch'])
                    ->run();
            /* @var $result Result */
            if (!$result) {
                $this->yell('Git update failed!');
                return;
            } else {
                try {
                    //migrate db
                    $this->dbMigrate();
                    //$this->dbMigrate(1, 'hired');
                } catch (Exception $ex) {
                    $this->yell('DB Migration failed: [' . $ex->getMessage() . ']');
                    return;
                }
                $result = $this->taskGitStack()
                        ->push('origin', $config['branch'])
                        ->run();

                if (!$result || !$result->wasSuccessful()) {
                    $this->yell('Git update failed!');
                    return;
                }
            }
        } else {
            $this->say('Skipping push');
        }



        $servers = $config['server'];
        if (empty($servers)) {
            $this->yell('Deployment fail, no server specified for : [' . $env . ']');
            return -1;
        }
        if (!is_array($servers)) {
            $servers = [$servers];
        }

        $serverTasks = array();
        //change to app folder
        $serverTasks[] = "export TB_ENV='{$env}'";

        $serverTasks[] = "cd {$config['app_path']}";

        //check out branh
        $serverTasks[] = $this->taskGitStack()
                ->checkout($config['branch'])
                ->pull();

        //run composer install 
        //run db:migrate on default
        $serverTasks[] = 'vendor/bin/robo db:migrate';
        //$serverTasks[] = $this->taskComposerInstall($config['composer_path'] ? : null);
        $serverTasks[] = 'composer install';
        //run composer update
        //$serverTasks[] = $this->taskComposerUpdate($config['composer_path'] ? : null);
        $serverTasks[] = 'composer update';
        //$serverTasks[] = 'whoami';
        //@TODO run other commands, like css compressiion, js, etc
        if (!$identityFile) {
            $identityFile = $this->getIdentityFile();
        }

        if (!$identityFile || !file_exists($identityFile)) {
            $this->yell('No valid identity file was found!');
            return;
        }

        foreach ($servers as $server) {
            $parts = explode(':', $server);
            $port = count($parts) > 1 ? $parts[1] : $config['port'];
            $this->say('Deploying to server: ' . $parts[0] . ' via port : ' . $port);
            $taskSsh = $this->taskSshExec($parts[0], $options['user'] ? : $config['user']);

            /* @var $taskSsh SshExecTask */
            $taskSsh->port($port)
                    ->identityFile($identityFile)
                    ->printed(true)
            ;

            //add tasks to run on server
            foreach ($serverTasks as $serverCmd) {
                $taskSsh->exec($serverCmd);
            }

            //fix for windows:
            $command = str_replace('\'', '"', $taskSsh->getCommand());
            $this->say('Converted command to : `' . $command . '`');
            $result = $this->taskExec($command)->run();
            //run as raw command

            if (!$result || !$result->wasSuccessful()) {
                $this->yell('Deployment failed to server!');
                return false;
            } else {
                $this->say('Deployment done');
            }
        }

        return true;
    }

    /**
     * 
     * Deploys application to testing servers.
     * @param string $identityFile  the path to the identity file for deployment.
     * @option $skip-push skips commit/pull/push cycle before commit
     * @option $user the user to use when logging to SSH
     */
    public function deployTesting($identityFile = null, $options = ['skip-push' => true, 'user' => 'wwwuser']) {
        return $this->taskDeploy('testing', $identityFile, $options);
    }

    /**
     * 
     * Deploys application to production servers.
     * @param string $identityFile  the path to the identity file for deployment.
     * @option $skip-push skips commit/pull/push cycle before commit
     * @option $user the user to use when logging to SSH
     */
    public function deployProduction($identityFile = null, $options = ['skip-push' => true, 'user' => 'wwwuser']) {
        return $this->taskDeploy('production', $identityFile, $options);
    }

    protected function getHomeDir() {
        return strncasecmp(PHP_OS, 'WIN', 3) == 0 ? getenv('USERPROFILE') : getenv('HOME');
    }

    protected function getIdentityFile() {
        $homeDir = $this->getHomeDir();
        $base = $homeDir . DIRECTORY_SEPARATOR . '.ssh/' . DIRECTORY_SEPARATOR;
        $tries = ['id_rsa', 'id_dsa', 'deployment_key'];
        foreach ($tries as $trial) {
            $file = $base . $trial;
            if (file_exists($file)) {
                return $file;
            }
        }
        return null;
    }

    public function moveFiles() {
        $migrate = new MigrateToS3();
        $migrate->start();
    }

    /**
     * 
     * Build an on premise package
     * @param string $package the name of the package you wish to build.
     * @option $publish Determines if the built package will be publish.
     * @option $version-bump determines if the version should be bumped to the next fix.
     * @option $build-version states the on premise version to use when releasing, if not set, version is auto
     *          loaded from the BUILD_VERSION file.
     * @option $server-url specifies the server URL that will be set in packageinfo.json this URL is 
     *          used to communicate with between Onpremise versions and Talentbase.
     */
    public function buildPackage($package = null, $options = ['publish' => false, 'build-version' => null, 'version-bump' => 'yes', 'server-url' => 'https://portal.talentbase.ng']) {

        require_once 'package/packager.php';

        $packageTask = new PackagerTasks();

        if (isset($options['version-bump'])) {
            $packageTask->setVersionBump($this->_getBoolean($options['version-bump']));
        }

        if (is_string($options['build-version']) && strlen($options['build-version'])) {
            packager\OnsiteVersions::setBuildVersion($options['build-version']);
        }

        $packageTask
                ->serverUrl($options['server-url'])
                ->build($package, $options['publish']);
    }

    private function _getBoolean($val) {
        return ($val = trim($val)) && !in_array(strtolower($val), ['no', 'false', 'n']);
    }

    /**
     * 
     * Bump version.
     * @param string $newVersion the version to bump to, bumps to the next fix if this is not suppllied.
     */
    public function buildVersionBump($newVersion = null) {
        require_once 'package/packager.php';
        $packageTask = new PackagerTasks();
        $packageTask->versionBump($newVersion);
    }

    /**
     * 
     * Updates on-premise version
     * @option $auto-update when set to true, the application is automatically update.
     * 
     */
    public function opUpdate($options = ['auto-update' => 'true']) {
        require_once __DIR__ . '/setup/tasks/onpremise.php';
        $task = new OnpremiseUpdateTask($this);
        $result = $task
                ->autoUpdate($this->_getBoolean($options['auto-update']))
                ->run();

        $this->say($result->getMessage());
        $this->say('Cleaning up now');
        $task->cleanUp();
    }

    /**
     * 
     * backs up on-premise version
     * @option $dir the backup directory.
     * 
     */
    public function opBackup($options = ['dir' => '/backup']) {
        require_once __DIR__ . '/setup/tasks/onpremise.php';
        $task = new OnpremiseBackupTask($this);
        $result = $task
                ->setBackupDir($options['dir'])
                ->run();

        $this->say($result ? $result->getMessage() : '');
    }

}
