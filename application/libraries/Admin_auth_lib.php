<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'classes/utils/IpaddressLocator.php';

/**
 * Eduportal
 * Company Admin auth lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage Admin_auth
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright Â© 2015 EduPortal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Admin_auth_lib {

    private $ad_prefix = '_ed_adm_';
    private $CI;
    private $message;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        // Load libraries
        $this->CI->load->library(array('session', 'user_agent'));

        // Load models
        $this->CI->load->model('ed/admin_model', 'admin_model');
        // Load helper
        $this->CI->load->helper('url');

        // Set user email value form sessions
        $this->email_user = $this->CI->session->userdata($this->ad_prefix . 'email');
        $this->admin_id = $this->CI->session->userdata($this->ad_prefix . 'id_admin');



        $this->db = $this->CI->load->database('', true);
    }

    /**
     * Login method
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    public function login($email, $password) {
        $password = $this->encrypt($password);

        // Fetch user data from database by email and password
        $result = $this->CI->admin_model->fetch_account(array('email' => $email, 'password' => $password));
        if (!$result) {
            // User does not exists
            return FALSE;
        }

        // Define params
        $status = $result[0]->status;

        // Check if user status is ACTIVE
        if ($status == USER_STATUS_ACTIVE) {

            $key = sha1($result[0]->email . '_' . $status);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                $this->ad_prefix . 'admin_id' => $result[0]->admin_id,
                $this->ad_prefix . 'email' => $result[0]->email,
                $this->ad_prefix . 'status' => $status,
                $this->ad_prefix . 'display_name' => $result[0]->first_name,
                $this->ad_prefix . 'last_name' => $result[0]->last_name,
                $this->ad_prefix . 'k' => $key
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
        }

        // Return array with user data
        return array(
            "status" => $status
        );
    }

    /**
     * Redirect to login page if user not logged in.
     * 
     * @access public
     * @return void
     */
    public function check_login() {

        if (!$this->logged_in()) {
            redirect(site_url('eadmin/login'), 'refresh');
        }
    }

    /**
     * Check if user logged in
     *
     * @access public
     * @return bool
     * */
    public function logged_in() {

        $cdata = array(
            'email' => $this->CI->session->userdata($this->ad_prefix . 'email'),
            'status' => $this->CI->session->userdata($this->ad_prefix . 'status')
        );

        foreach ($cdata as $data) {
            if (trim($data) == '') {
                return false;
            }
        }

        $s_k = $this->CI->session->userdata($this->ad_prefix . 'k');
        $c_k = sha1($cdata['email'] . '_' . $cdata['status']);

        if ($s_k != $c_k) {
            return false;
        }

        return true;
    }

    /**
     * Encrypt string to sha1 
     * 
     * @access public
     * @param string $str
     * @return string
     */
    public function encrypt($str) {
        return sha1($str);
    }

    /**
     * Get session variable value assigned to user. 
     * 
     * @access public
     * @param string $item
     * @return mixed (bool | string)
     */
    public function get($item) {

        if (!$this->logged_in()) {
            return false;
        }

        return $this->CI->session->userdata($this->ad_prefix . $item);
    }

    public function logout() {
        $this->log_admin_action('Admin logged out successfully.', 2301);
        // Destroy current user session
        $this->CI->session->sess_destroy();
    }

    /**
     * Redirect to user's access denied page, if user have not permission.
     * 
     * @access public 
     * @param type $id_perm 
     * @return void
     */
    public function check_perm($id_perm) {
        if (!$this->have_perm($id_perm)) {
            redirect(site_url('eadmin/access_denied'), 'refesh');
        }
    }

    public function log_admin_action($message, $id_log, $url = null, $token = 0, $proxyUser = null, $proxyCompany = null, $schoolId = 0) {

        $iplocator = \utils\IpaddressLocator::fetchIpaddressCityCountry($this->CI->input->ip_address());
        $ip = json_decode($iplocator);

        if ($ip->status == 'success') {
            $city = $ip->city;
            $country = $ip->country;
        } else {
            $city = '';
            $country = '';
        }

        if ($this->get('admin_id') != 0) {
            $this->db->insert('ed_user_logs', array(
                'admin_id' => $this->get('admin_id'),
                'message' => $message,
                'type' => $id_log,
                'log_date' => date('Y-m-d H:i:s'),
                'school_id' => $schoolId,
                'ip_address' => ip2long($this->CI->input->ip_address()),
                'user_agent' => $this->CI->agent->agent_string(),
                'session_id' => $this->CI->session->userdata('session_id'),
                'city' => $city,
                'country' => $country,
                'url' => $url,
                'token' => $token,
                'proxy_user_string' => $proxyUser,
                'proxy_company_string' => $proxyCompany
            ));
        }
    }

}

// end class: admin_auth_lib
