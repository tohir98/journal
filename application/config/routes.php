<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'admin/dashboard';
$route['default_controller'] = 'welcome/landing';
//$route['default_controller'] = 'users/Users_controller';
$route['login']  = 'users/users_controller/login';
$route['logout']  = 'users/users_controller/logout';
$route['verify_email']  = 'users/users_controller/verify_email';
$route['verify_email/(:num)']  = 'users/users_controller/verify_email/$1';
$route['verify_email/(:num)/(:any)']  = 'users/users_controller/verify_email/$1/$2';
$route['verify_email/(:num)/(:any)/(:any)']  = 'users/users_controller/verify_email/$1/$2/$3';
$route['admin']  = 'users/users_controller/super_admin';
$route['admin/dashboard']  = 'users/admin_controller/dashboard';
$route['account/dashboard']  = 'users/admin_controller/dashboard';
$route['account/edit_profile']  = 'users/users_controller/edit_profile';


$route['setup']  = 'setup/setup_controller';
$route['setup/(:any)']  = 'setup/setup_controller/$1';
$route['setup/(:any)/(:any)']  = 'setup/setup_controller/$1/$2';
$route['setup/(:any)/(:any)/(:any)']  = 'setup/setup_controller/$1/$2/$3';


$route['forgot_password']  = 'users/users_controller/forgot_password';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['journals']  = 'journal/journal_controller';
$route['journals/(:any)']  = 'journal/journal_controller/$1';
$route['journals/(:any)/(:any)']  = 'journal/journal_controller/$1/$2';
$route['journals/(:any)/(:any)/(:any)']  = 'journal/journal_controller/$1/$2/$3';

$route['users']  = 'users/users_controller';
$route['users/(:any)']  = 'users/users_controller/$1';
$route['users/(:any)/(:any)']  = 'users/users_controller/$1/$2';
