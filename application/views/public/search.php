<br>
<h3 style="text-align: center; font-weight: bold"><?= ucfirst($_GET['theme']); ?></h3>

<section>

    <div class="row">
        <div class="col-md-2">
            &nbsp;
        </div>
        <div class="col-md-8">
            <?php
            if (!empty($results)):
                $sn = 0;
                foreach ($results as $result):
                    ?>
                    <div class="well" style="font-size: large">
                        <?= ++$sn . '. ' . ucfirst($result->author) ?>. 
                        (<?= $result->year ?>).
                        <i><b><?= ucfirst($result->title) ?></b></i>. 
                         <?= trim($result->volume)?>(<?= trim($result->issues) ?>),
                        <?= ucfirst($result->pages) ?> <br>
                        <a href="<?= $result->url ?>" target="_blank"><?= $result->url ?> </a><br>
                        <?= $result->abstract ?>
                    </div>
            <hr>
                    <?php
                endforeach;
            else:
                echo show_no_data('No group matched your search criteria in our archive');
            endif;
            ?>
        </div>
        <div class="col-md-2">
            &nbsp;
        </div>
    </div>
</section>