<style>
    .form_control{
        width: 80%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
    }
</style>

<body class="skin-blue sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo"><?= BUSINESS_NAME ?></a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">

                <a href="<?= site_url('/login') ?>" class="pull-right" style="
                   padding: 14px;
                   color: white;
                   display: block;
                   border: 1px solid blueviolet;
                   background: #324292;
                   ">login</a>
            </nav>
        </header>

        <div class="content-wrapper" style="min-height: 1068px;">
            <h1 style="text-align: center; color: #9E2E04;
                font-weight: bold;">Centre for Malaysian Indigenous Studies (CMIS) <br>
                University of Malaya</h1>

            <section class="content" style="background-image: url(/img/jour_bg1.jpg); background-repeat: no-repeat; background-size: cover; color: #fff">

                <p>&nbsp;</p>
                <div>
                    <!--<h1 style="font-family: arial-black">Malay Journal<br> Database</h1>-->
                </div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </section>

            <div class="row" style="margin-top: 20px;">
                <div class="col-md-2">
                    &nbsp;
                </div>
                <div class="col-md-8">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="<?= site_url('welcome') ?>#articles" aria-expanded="false">Articles</a></li>
                            <li class=""><a href="<?= site_url('welcome') ?>#books" aria-expanded="true">Books</a></li>
                            <li class=""><a href="<?= site_url('welcome') ?>#book_chapters" aria-expanded="true">Book Chapters</a></li>
                            <li class=""><a href="<?= site_url('welcome') ?>#conferences" aria-expanded="true">Conferences</a></li>

                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    &nbsp;
                </div>
            </div>


            <section>
               

                <div class="row">
                    <div class="col-md-2">
                        &nbsp;
                    </div>
                    <div class="col-md-8">
                        <?php
                        if (!empty($results)):
                            $sn = 0;
                            foreach ($results as $result):
                                ?>
                                <div class="well" style="font-size: large">
                                    <?= ++$sn .', ' . ucfirst($result->author) ?>, 
                                    <i><b><?= ucfirst($result->title) ?></b></i>, 
                                    <?= $result->year ?> <?= $result->volume ?>, 
                                    (<?= $result->issues ?>), 
                                    <?= ucfirst($result->publisher) ?>, 
                                    <?= ucfirst($result->group_name) ?>, 
                                    <?= ucfirst($result->category) ?>, 
                                    <a href="<?= $result->url ?>" target="_blank"><?= $result->url ?> </a>
                                </div>
                                <?php
                            endforeach;
                        else:
                            echo show_no_data('No group matched your search criteria in our archive');
                        endif;
                        ?>
                    </div>
                    <div class="col-md-2">
                        &nbsp;
                    </div>
                </div>
            </section>

        </div>
        <?php include APPPATH . 'views/templates/footer.php'; ?>
    </div>
</body>

