<style>
    .form_control{
        width: 80%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
    }
</style>
<body class="skin-blue sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <a href="../../index2.html" class="logo"><?= BUSINESS_NAME ?></a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">

                <a href="<?= site_url('/login') ?>" class="pull-right" style="
                   padding: 14px;
                   color: white;
                   display: block;
                   border: 1px solid blueviolet;
                   background: #324292;
                   ">login</a>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 1068px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Search Results
                    <small>....</small>
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="well">
                    <form method="post" role="form" action="<?= site_url('welcome/search') ?>">
                        <h3 style="text-align: center">Malay Academic Database</h3>
                        <center>
                            <div> 
                                <select required name="txtSearch" class="form_control" title="Pls supply search group">
                                    <option value="" selected>Select Group</option>
                                    <?php if (!empty($groups)):
                                        foreach ($groups as $group):
                                            ?>
                                            <option value="<?= $group->group_name ?>"><?= $group->group_name ?></option>
                                        <?php endforeach;
                                    endif;
                                    ?>
                                </select>
                                <button class="btn btn-primary btn-flat btn-sm">Search</button> </div>
                        </center>

                        <p style="text-align: center"> Search for Articles, books, conferences and other research materials...  </p>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (!empty($results)):
                            foreach ($results as $result):
                                ?>
                                <div class="well" style="font-size: 14pts">
                                    <?= ucfirst($result->author) ?>, 
                                    <i><b><?= ucfirst($result->title) ?></b></i>, 
                                    <?= $result->year ?> <?= $result->volume ?>, 
                                    (<?= $result->issues ?>), 
                                    <?= ucfirst($result->publisher) ?>, 
        <?= ucfirst($result->group_name) ?>, 
                                <?= ucfirst($result->category) ?>, 
                                    <a href="<?= $result->url ?>" target="_blank"><?= $result->url ?> </a>
                                </div>
                                <?php
                            endforeach;
                        else:
                            echo show_no_data('No group matched your search criteria in our archive');
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div><!-- /.content-wrapper -->

<?php include APPPATH . 'views/templates/footer.php'; ?>

    </div>

</body>
