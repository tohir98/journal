
<body class="skin-blue sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <a href="../../index2.html" class="logo"><?= BUSINESS_NAME ?></a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                
                <a href="<?= site_url('/login')?>" class="pull-right" style="
                   padding: 14px;
                   color: white;
                   display: block;
                   border: 1px solid blueviolet;
                   background: #324292;
                   ">login</a>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 1068px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Journals
                    <small>....</small>
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#articles" data-toggle="tab" aria-expanded="false">Articles</a></li>
                                <li class=""><a href="#books" data-toggle="tab" aria-expanded="true">Books</a></li>
                                <li class=""><a href="#book_chapters" data-toggle="tab" aria-expanded="true">Book Chapters</a></li>
                                <li class=""><a href="#conferences" data-toggle="tab" aria-expanded="true">Conferences</a></li>

                                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="articles">
                                    <?php include APPPATH . 'views/journals/_articles.php'; ?>
                                </div><!-- /.tab-pane -->
                                <div class="tab-pane" id="books">
                                    <?php include APPPATH . 'views/journals/books/_books.php'; ?>
                                </div><!-- /.tab-pane -->
                                <div class="tab-pane" id="book_chapters">
                                    <?php include APPPATH . 'views/journals/book_chapter/_book_chapter.php'; ?>
                                </div><!-- /.tab-pane -->
                                <div class="tab-pane" id="conferences">
                                    <?php include APPPATH . 'views/journals/conference/_conference.php'; ?>
                                </div><!-- /.tab-pane -->
                            </div><!-- /.tab-content -->
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /.content-wrapper -->

        <?php include APPPATH . 'views/templates/footer.php'; ?>

    </div>

</body>
