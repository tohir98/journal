<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $login_title ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?= site_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?= site_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?= site_url(); ?>css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?= site_url(); ?>plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <h1 style="">
                    <a href="<?php echo site_url(); ?>">
                        <b>Agent</b>CRM
                    </a>
                </h1>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><b>Reset</b> Password <br>
                    <?php if ($error_message !== ''): ?>
                        <span style="color: #D6498A;"><?= $error_message; ?></span> 
                    <?php elseif ($success_message !== ''): ?>
                        <span style="color: #D6498A;"><?= $success_message; ?></span> 
                    <?php endif; ?>

                </p>
                <form action="" method="post">
                    <div class="form-group has-feedback">
                        <input required type="password" name="password" id="password" class="form-control" placeholder="Password" value=""/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                    </div>
                    <div class="form-group has-feedback">
                        <input required type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password" value="" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                    </div>
                    <div class="row">

                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat ">Reset</button>
                        </div><!-- /.col -->
                    </div>
                </form>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.3 -->
        <script src="<?= site_url(); ?>plugins/jQuery/jQuery-2.1.3.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?= site_url(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?= site_url(); ?>plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>