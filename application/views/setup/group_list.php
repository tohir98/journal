<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Groups</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class=""><a href="<?= site_url('setup/categories') ?>" aria-expanded="false">Categories</a></li>
                    <li class="active"><a href="<?= site_url('setup/groups') ?>" aria-expanded="true">Groups</a></li>

                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-flat" href="#addGroup" data-toggle="modal"> 
                                    <i class="fa fa-plus-circle"></i> Add New Group
                                </a>
                            </div>
                            <div class="box-body">
                                <?php
//                                var_dump($groups);
                                if (!empty($groups)):
                                    foreach ($groups as $group):
                                        ?>
                                        <div class="box box-default">
                                            <div class="box-header with-border">
                                                <h6 class="box-title">
                                                    <b><?= $group['group_name'] ?></b> &nbsp;| &nbsp;
                                                    <a href="#" onclick="return false;" data-class_id ="<?= $group['group_id'] ?>" data-class_name ="<?= $group['group_name'] ?>" class="classarm label label-waring">Add Sub Group</a> | 
                                                    <a href="#" onclick="return false;" class="edit">
                                                        <i class="fa fa-edit"></i>
                                                        Edit
                                                    </a> |
                                                    <a href="<?= site_url('/setup/delete_group/' . $group['group_id']) ?>" onclick="return false;" class="deleteGroup" data-message="Are you sure you want to delete the selected group?">
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a> 
                                                </h6>
                                                <div class="box-tools pull-right">
                                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                </div><!-- /.box-tools -->
                                            </div><!-- /.box-header -->
                                            <div class="box-body">
                                                <?php
                                                if (!empty($group['sub_group'])):
                                                    $cnt = 0;
                                                    ?>
                                                    <table class="table table-condensed table-striped table-bordered">
                                                        <tr>
                                                            <th>S/N</th>
                                                            <th>Sub Group</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        <?php foreach ($group['sub_group'] as $sub): ?>
                                                            <tr>
                                                                <td><?= ++$cnt; ?></td>
                                                                <td><?= ucfirst($sub['sub_group']) ?></td>
                                                                <td>
                                                                    <a class="groupToggle" data-enabled="<?= intval($sub['status']) ?>" href="<?= site_url('/setup/edit_subgroup_status/' . $sub['sub_group_id'] . '/' . $sub['status']); ?>">
                                                                        <?= $sub['status'] ? 'Disable' : 'Enable' ?>
                                                                    </a> |
                                                                    <a class="deleteGroup" href="<?= site_url('/setup/delete_sub_group/' . $sub['sub_group_id']); ?>" data-message="If you delete this sub group, you will not be able to assign it, Are you sure you want to continue?">
                                                                        Delete
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach;
                                                        ?>
                                                    </table>
                                                <?php endif;
                                                ?>


                                            </div><!-- /.box-body -->
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="addGroup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Group</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="class_name">Group Name</label>
                        <input required type="text" class="form-control" id="group_name" name="group_name" placeholder="Group Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-classarm" ng-app="class" ng-controller="armCtrl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">New Class Arm</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/setup/add_sub_group') ?>">
                <div class="modal-body">
                    <table class="table">
                        <tr ng-repeat="status in data.statuses">
                            <td>Sub Group {{$index + 1}}</td>
                            <td><input required type="text" class="form-control" name="sub_group[{{$index}}]" value=""></td>
                            <td>
                                <a href="#" title="Remove this step" onclick="return false;" ng-click="removeItem($index)">
                                    <i class="fa fa-fw fa-trash-o"></i> remove
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <a title="Click here to add" class="btn btn-warning btn-sm pull-right" onclick="return false;" ng-click="addItem()">
                                    <i class="icons icon-plus"></i>
                                    Add more
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="group_id" id="group_id" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.groupToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this sub group, you will not be able to assign it, Are you sure you want to continue? ' : 'Are you sure you want to enable selected sub group ?';
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });

        $('.deleteGroup').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var msg_ = $(this).data('message');
            var message = msg_;
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    $('.classarm').click(function () {
        var class_name_ = $(this).data('class_name');
        var group_id_ = $(this).data('class_id');

        var text_ = 'Sub Group for ' + class_name_;

        $('#class_arm_title').text(text_);
        $('#group_id').val(group_id_);
        $('#modal-classarm').modal();
    });

    var classApp = angular.module('class', []);

    classApp.controller('armCtrl', function ($scope) {

        $scope.data = {statuses: []};

        $scope.blankResult = {status: ''};

        $scope.addItem = function () {
            $scope.data.statuses.push(angular.copy($scope.blankResult));
        };

        $scope.getSittingNo = function () {
            return $scope.data.sittings.length;
        };

        $scope.removeItem = function (idx) {
            if ($scope.data.statuses.length > 0) {
                $scope.data.statuses.splice(idx, 1);
            }
            $scope.check();
        };

        $scope.check = function () {
            if ($scope.data.statuses.length == 0) {
                $scope.addItem();
            }
        };

        $scope.check();



    });
</script>