<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<?php include 'header.php'; ?>

<body class="skin-blue">
    <div class="wrapper">
        <?php include 'top_menu.php'; ?>

        <?php include 'left_side_bar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?php
            if (is_callable($page_content)) {
                call_user_func($page_content);
            }
            ?>
        </div>
        <!-- /.content-wrapper -->
        <?php include 'footer.php'; ?>
</body>
</html>