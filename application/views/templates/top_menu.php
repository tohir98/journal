<header class="main-header">
    <!--<a href="#" class="logo"><?= $school_name ?></a>-->
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <span style="display: block;
    float: left;
    height: 50px;
    font-size: 20px;
    line-height: 50px;
    text-align: center;
        color: #fff;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    padding: 0 15px;
    font-weight: 300;"><?= $school_name ?></span>
        <a href="<?= $logout_url ?>" class="pull-right" style="
           padding: 14px;
           color: white;
           display: block;
           border: 1px solid blueviolet;
           background: #324292;
           ">&nbsp;</a>
    </nav>
</header>