<div class="box">
    <div class="box-header">
        <?php if ($this->user_auth_lib->get('user_id')): ?>
            <h3 class="box-title">
                <a class="btn btn-block btn-primary pull-right" href="<?= site_url('/journals/add_article'); ?>">
                    Add New Journal
                </a>
            </h3>
        <?php endif; ?>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php
        if (!empty($articles)):
            ?>
            <table class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Year</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Group</th>
                        <th>Sub Group</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($articles as $article): ?>
                        <tr>
                            <td><a href="<?= site_url('journals/view/1/'. $article->article_id)?>">
                                <?= ucfirst($article->author); ?>
                                </a></td>
                            <td> <?= $article->year ?> </td>
                            <td><?= ucfirst($article->title) ?></td>
                            <td><?= $article->category ?></td>
                            <td><?= $article->sub_category ?></td>
                            <td><?= $article->group_name ?></td>
                            <td><?= $article->sub_group ?></td>

                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?= site_url('journals/view/1/'. $article->article_id)?>" >View</a></li>
                                        <?php if ($this->user_auth_lib->get('user_id')): ?>
                                            <li><a href="#" onclick="return false;" class="edit">Edit</a></li>
                                            <li><a href="<?= site_url('/journals/delete/1/' . $article->article_id) ?>" onclick="return false;" class="deleteArticle" data-message="Are you sure you want to delete the selected journal?" title="Delete Journal">Delete</a></li>
                                            <?php endif; ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php
        else:
            $msg = "No article has been added. <a href=" . site_url('/journals/add_article') . ">Click here to add one.</a>";
            echo show_no_data($msg);
        endif;
        ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->