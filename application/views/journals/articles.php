<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Articles
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Articles</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?= site_url('journals/articles') ?>" aria-expanded="false">Journals</a></li>
                    <li class=""><a href="<?= site_url('journals/books') ?>" aria-expanded="true">Books</a></li>
                    <li class=""><a href="<?= site_url('journals/book_chapters') ?>" aria-expanded="true">Book Chapters</a></li>
                    <li class=""><a href="<?= site_url('journals/conferences') ?>" aria-expanded="true">Conferences</a></li>
                    <li class=""><a href="<?= site_url('journals/other') ?>" aria-expanded="true">Media</a></li>

                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <?php include '_articles.php'; ?>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>