<section class="content-header">
    <h1>
        Authors
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Authors</li>
    </ol>
</section>

<?php $lists = ['articles' => 1, 'books' => 2, 'book_chapter' => 3, 'conference' => 4, 'others' => 5]; ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body"> 
                    <?php if (!empty($authors)): ?>
                        <table class="table table-condensed table-bordered dataTable">
                            <tr>
                                <th>SN</th>
                                <th>Author</th>
                            </tr>
                            <?php $sn = 0;
                            foreach ($authors as $aut): ?>
                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td>
                                        <a href="<?= site_url('journals/view/'.$lists[$aut->type] . '/' . $aut->j_id) ?>">
                                        <?= $aut->author; ?>
                                        </a>
                                    </td>
                                </tr>
                        <?php endforeach; ?>
                        </table>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


