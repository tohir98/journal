<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Index
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Index</li>
    </ol>
</section>

<?php $lists = ['articles' => 1, 'books' => 2, 'book_chapter' => 3, 'conference' => 4, 'others' => 5]; ?>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body"> 
                    <table class="table">
                        <tr>
                            <td>
                                <b>A</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'a'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>B</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'b'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>C</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'c'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>D</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'd'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>E</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'e'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>F</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'f'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>G</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'g'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>H</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'h'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>I</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'i'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>J</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'j'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>K</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'k'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>L</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'l'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>M</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'm'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>N</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'n'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>O</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'o'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>P</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'p'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>Q</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'q'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>R</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'r'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>S</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 's'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>T</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 't'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>U</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'u'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>V</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'v'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>W</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'w'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>X</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'x'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Y</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'y'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>
                                <b>Z</b>
                                <?php
                                if (!empty($authors)):
                                    foreach ($authors as $author):
                                        if (strtolower(substr($author->author, 0, 1)) == 'z'):
                                            ?>
                                            <div><a href="<?= site_url('journals/view/'.$lists[$author->type] . '/' . $author->j_id) ?>"><?= $author->author ?></a></div>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                            <td>&nbsp; </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>