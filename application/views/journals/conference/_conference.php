<div class="box">
    <div class="box-header">
        <?php if ($this->user_auth_lib->get('user_id')): ?>
            <h3 class="box-title">
                <a class="btn btn-block btn-primary pull-right" href="<?= site_url('/journals/add_conference'); ?>">
                    Add New
                </a>
            </h3>
        <?php endif; ?>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php
        if (!empty($conferences)):
            ?>
            <table id="example1" class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Year</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Group</th>
                        <th>Sub Group</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($conferences as $conference): ?>
                        <tr>
                            <td>
                                <a href="<?= site_url('journals/view/4/' . $conference->conference_id) ?>" >
                                    <?= ucfirst($conference->author); ?>
                                </a>
                            </td>
                            <td> <?= $conference->year ?> </td>
                            <td><?= ucfirst($conference->title) ?></td>
                            <td><?= $conference->category ?></td>
                            <td><?= $conference->sub_category ?></td>
                            <td><?= $conference->group_name ?></td>
                            <td><?= $conference->sub_group ?></td>

                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?= site_url('journals/view/4/' . $conference->conference_id) ?>" >View</a></li>
                                        <?php if ($this->user_auth_lib->get('user_id')): ?>
                                            <li><a href="#" onclick="return false;" class="edit">Edit</a></li>
                                            <li><a href="<?= site_url('/journals/delete/4/' . $conference->conference_id) ?>" onclick="return false;" class="deleteArticle" data-message="Are you sure you want to delete the selected conference?" title="Delete Journal">Delete</a></li>
                                            <?php endif; ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php
        else:
            $msg = "No conference has been added. <a href=" . site_url('/journals/add_conference') . ">Click here to add one.</a>";
            echo show_no_data($msg);
        endif;
        ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->