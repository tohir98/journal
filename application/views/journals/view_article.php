<?= show_notification(); ?>
<section class="content-header">
    <a href="<?= $this->input->server('HTTP_REFERER') ?>" class="btn btn-warning btn-flat btn-sm">
        <i class="fa fa-chevron-left"></i> Back
    </a>
    <br>
    <h1>
        View Article
    </h1>

    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Articles</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-2">
            &nbsp;
        </div>
        <div class="col-md-8">
            <?php
            $sn = 0;
            if (!empty($article)):
                ?>
                <div class="well" style="font-size: large">
                    <?= ++$sn ?>. 
                    <?= ucfirst($article[0]->author) ?>. 
                    (<?= $article[0]->year ?>). 
                    <i><b><?= ucfirst($article[0]->title) ?></b></i>. 
                    <?= $article[0]->volume?>(<?= trim($article[0]->issues) ?>), 
                    <?= ucfirst($article[0]->pages) ?><br>
                    <a href="<?= $article[0]->url ?>" target="_blank"><?= $article[0]->url ?> </a><br>
                    <?= ucfirst($article[0]->abstract) ?>
                </div>
                <?php
            endif;
            ?>
        </div>
        <div class="col-md-2">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped table-condensed table-hover table-responsive">
<!--                        <tr>
                            <td style="width: 25%;">Author</td>
                            <td><?= ucfirst($article[0]->author); ?></td>
                        </tr>
                        <tr>
                            <td>Year</td>
                            <td><?= $article[0]->year; ?></td>
                        </tr>
                        <tr>
                            <td>Title</td>
                            <td><?= ucfirst($article[0]->title); ?></td>
                        </tr>
                        <tr>
                            <td>Volume</td>
                            <td><?= $article[0]->volume; ?></td>
                        </tr>
                        <tr>
                            <td>Issues</td>
                            <td><?= $article[0]->issues; ?></td>
                        </tr>-->
<!--                        <tr>
                            <td>Pages</td>
                            <td><?= $article[0]->pages; ?></td>
                        </tr>
                        <tr>
                            <td>ISBN</td>
                            <td><?= $article[0]->isbn; ?></td>
                        </tr>
                        <tr>
                            <td>URL</td>
                            <td><a href="<?= $article[0]->url; ?>"><?= $article[0]->url; ?></a></td>
                        </tr>-->
                        <tr>
                            <td>Group</td>
                            <td><?= $article[0]->group_name; ?></td>
                        </tr>
                        <tr>
                            <td>Sub Group</td>
                            <td><?= $article[0]->sub_group; ?></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td><?= $article[0]->category; ?></td>
                        </tr>
                        <tr>
                            <td>Sub Category</td>
                            <td><?= $article[0]->sub_category; ?></td>
                        </tr>
                        <tr>
                            <td>Keywords</td>
                            <td><?= $article[0]->keywords; ?></td>
                        </tr>
                        <tr>
                            <td>Abstract</td>
                            <td><p style="text-align: justify">
                                    <?= $article[0]->abstract; ?>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>