<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Journals
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Other</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                     <li class=""><a href="<?= site_url('journals/articles') ?>" aria-expanded="false">Journals</a></li>
                    <li class=""><a href="<?= site_url('journals/books') ?>" aria-expanded="true">Books</a></li>
                    <li class=""><a href="<?= site_url('journals/book_chapters') ?>" aria-expanded="true">Book Chapters</a></li>
                    <li class=""><a href="<?= site_url('journals/conferences') ?>" aria-expanded="true">Conferences</a></li>
                    <li class="active"><a href="<?= site_url('journals/other') ?>" aria-expanded="true">Media</a></li>

                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="box">
                            <div class="box-header">
                                <a href="<?= site_url('journals/other') ?>" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-chevron-left"></i> Back</a>
                                <h3>
                                    Add New </h3>
                            </div>
                            <div class="box-body" ng-app="journal">
                                <form id="frm" role="form" method="post" ng-controller="journalCtrl">
                                    <table class="table" >
                                        <tr>
                                            <td>Author</td>
                                            <td><input required type="text" class="form-control" id="author" name="author" placeholder="Author" value=""></td>
                                            <td>Year</td>
                                            <td><input required type="text" class="form-control" id="year" name="year" placeholder="Year" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>Title</td>
                                            <td><input required type="text" class="form-control" id="title" name="title" placeholder="Title" value=""></td>
                                            <td>Volume</td>
                                            <td><input required type="text" class="form-control" id="volume" name="volume" placeholder="Volume" value=""></td>
                                        </tr>

                                        <tr>
                                            <td>Issues</td>
                                            <td><input required type="text" class="form-control" id="issues" name="issues" placeholder="Issues" value=""></td>
                                            <td>Pages</td>
                                            <td><input required type="text" class="form-control" id="pages" name="pages" placeholder="Pages" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>URL</td>
                                            <td><input required type="text" class="form-control" id="url" name="url" placeholder="Url" value=""></td>
                                            <td>Isbn</td>
                                            <td><input required type="text" class="form-control" id="isbn" name="isbn" placeholder="Isbn" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>Category</td>
                                            <td>
                                                <select class="form-control" ng-model="category_id" id="category_id" name="category_id" ng-change="loadSubCategories()">
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    if (!empty($categories)):
                                                        $sel = '';
                                                        foreach ($categories as $category):
                                                            ?>
                                                            <option value="<?= $category->category_id ?>"><?= trim($category->category) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                            <td>Sub Category</td>
                                            <td>
                                                <div ng-if="subcategoryStatus !== ''">
                                                    {{subcategoryStatus}}
                                                </div>
                                                <div ng-if="subcategoryStatus === ''">
                                                    <select name="sub_category_id" id="sub_category_id" class="form-control" ng-model="sub_category_id">
                                                        <option ng-repeat="subcategory in subcategories" value="{{subcategory.id}}">
                                                            {{subcategory.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Group</td>
                                            <td>
                                                <select class="form-control" id="group_id" name="group_id" ng-model="group_id"  ng-change="loadSubGroups()">
                                                    <option value="">Select Group</option>
                                                    <?php
                                                    if (!empty($groups)):
                                                        $sel = '';
                                                        foreach ($groups as $group):
                                                            ?>
                                                            <option value="<?= $group->group_id ?>"><?= trim($group->group_name) ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </td>
                                            <td>Sub Group</td>
                                            <td>
                                                <div ng-if="subgroupStatus !== ''">
                                                    {{subgroupStatus}}
                                                </div>
                                                <div ng-if="subgroupStatus === ''">
                                                    <select name="sub_group_id" id="sub_group_id" class="form-control" ng-model="sub_category_id">
                                                        <option ng-repeat="subgroup in subgroups" value="{{subgroup.id}}">
                                                            {{subgroup.name}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Keywords</td>
                                            <td><input required type="text" class="form-control" id="keywords" name="keywords" placeholder="Keywords" value=""></td>
                                            <td>Abstract</td>
                                            <td><textarea required id="abstract" name="abstract" placeholder="Abstract" rows="4" style="width: 100%"></textarea></td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td><input required type="text" class="form-control" id="type" name="type" placeholder="Type" value=""></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary" ng-click="form_submit();">Save</button>
                                                <button type="reset" class="btn btn-warning">Clear</button>
                                            </td>
                                        </tr>
                                    </table>

                                </form>
                            </div>
                        </div>

                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>