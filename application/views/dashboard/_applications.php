<div class="box box-solid bg-blue-gradient">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="fa fa-calendar"></i>
        <h3 class="box-title">Agents (Applications in last 6 Months)</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            <!-- button with a dropdown -->

            <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <!--The calendar -->
        <table class="table">
            <thead>
                <tr>
                    <th>Agent Name</th>
                    <th>Branch</th>
                    <th>No of Applications</th>
                    <th>Converted</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($agents)):
                    foreach ($agents as $agent):
                        ?>
                        <tr>
                            <td><?= $agent->first_name ?></td>
                            <td><?= $agent->branch_name ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>

            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer text-black">
        <div class="row">
            <a href="#" class="btn btn-primary pull-right" style="margin-right: 10px;">View More</a>
        </div>
    </div>
</div><!-- /.box -->