<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of admin_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Agent_model $agent_model Description
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function dashboard() {
        $this->user_auth_lib->check_login();

        $data = array(
            'articles' => count($this->basic_model->fetch_all_records('articles')), 
            'books' => count($this->basic_model->fetch_all_records('books')), 
            'book_chapters' => count($this->basic_model->fetch_all_records('book_chapter')),
            'conferences' => count($this->basic_model->fetch_all_records('conference')),
        );
        $this->user_nav_lib->run_page('dashboard/dashboard', $data, 'Dashboard |'. BUSINESS_NAME);
    }

}
