<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * 
 * @property Basic_model $basic_model Description
 */

class Application_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/agent_model', 'a_model');
        $this->load->model('basic_model', 'basic_model');
    }
    
    public function new_application(){
        $this->user_auth_lib->check_login();
        
        $data = [
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        ];
        $this->user_nav_lib->run_page('application/new_application', $data, 'Application | Application CRM');
    }
    
    public function page_two(){
        $this->user_auth_lib->check_login();
        
        $data = [
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        ];
        $this->user_nav_lib->run_page('application/page_two', $data, 'Application | Application CRM');
    }

}
