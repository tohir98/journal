<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of journal_controller
 *
 * @author TOHIR
 * @property Basic_model $basic_model Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Journal_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('basic_model', 'basic_model');
    }

    public function articles() {
        $this->user_auth_lib->check_login();

        $data = [
            'articles' => $this->basic_model->fetch_journal('articles'),
        ];
        $this->user_nav_lib->run_page('journals/articles', $data, 'Articles | ' . BUSINESS_NAME);
    }

    public function add_article() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('articles', request_post_data())) {
                notify('success', 'Article saved successfully');
            } else {
                notify('error', 'Article could not be saved at moment, pls try again.');
            }
            redirect(site_url('journals/articles'));
        }

        $data = [
            'categories' => $this->basic_model->fetch_all_records('categories'),
            'sub_categories' => $this->basic_model->fetch_all_records('sub_categories'),
            'groups' => $this->basic_model->fetch_all_records('groups'),
            'sub_groups' => $this->basic_model->fetch_all_records('sub_groups'),
        ];

        $this->user_nav_lib->run_page('journals/add_article', $data, 'Add Article | ' . BUSINESS_NAME);
    }

    public function books() {
        $this->user_auth_lib->check_login();
        $data = [
            'books' => $this->basic_model->fetch_journal('books'),
        ];
        $this->user_nav_lib->run_page('journals/books/list', $data, 'Books | ' . BUSINESS_NAME);
    }

    public function add_book() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('books', request_post_data())) {
                notify('success', 'Book saved successfully');
            } else {
                notify('error', 'Book could not be saved at moment, pls try again.');
            }
            redirect(site_url('journals/books'));
        }

        $data = [
            'categories' => $this->basic_model->fetch_all_records('categories'),
            'sub_categories' => $this->basic_model->fetch_all_records('sub_categories'),
            'groups' => $this->basic_model->fetch_all_records('groups'),
            'sub_groups' => $this->basic_model->fetch_all_records('sub_groups'),
        ];

        $this->user_nav_lib->run_page('journals/books/add_book', $data, 'Add Book | ' . BUSINESS_NAME);
    }

    public function conferences() {
        $this->user_auth_lib->check_login();
        $data = [
            'conferences' => $this->basic_model->fetch_journal('conference'),
        ];
        $this->user_nav_lib->run_page('journals/conference/list', $data, 'Conference | ' . BUSINESS_NAME);
    }

    public function other() {
        $this->user_auth_lib->check_login();
        $data = [
            'others' => $this->basic_model->fetch_journal('others'),
        ];
        $this->user_nav_lib->run_page('journals/others/list', $data, 'Others | ' . BUSINESS_NAME);
    }

    public function add_other() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('others', request_post_data())) {
                notify('success', 'Entry saved successfully');
            } else {
                notify('error', 'Entry could not be saved at moment, pls try again.');
            }
            redirect(site_url('journals/other'));
        }

        $data = [
            'categories' => $this->basic_model->fetch_all_records('categories'),
            'sub_categories' => $this->basic_model->fetch_all_records('sub_categories'),
            'groups' => $this->basic_model->fetch_all_records('groups'),
            'sub_groups' => $this->basic_model->fetch_all_records('sub_groups'),
        ];

        $this->user_nav_lib->run_page('journals/others/add_other', $data, 'Add Others | ' . BUSINESS_NAME);
    }

    public function add_conference() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('conference', request_post_data())) {
                notify('success', 'Conference saved successfully');
            } else {
                notify('error', 'Conference could not be saved at moment, pls try again.');
            }
            redirect(site_url('journals/conferences'));
        }

        $data = [
            'categories' => $this->basic_model->fetch_all_records('categories'),
            'sub_categories' => $this->basic_model->fetch_all_records('sub_categories'),
            'groups' => $this->basic_model->fetch_all_records('groups'),
            'sub_groups' => $this->basic_model->fetch_all_records('sub_groups'),
        ];

        $this->user_nav_lib->run_page('journals/conference/add_conference', $data, 'Add Conference | ' . BUSINESS_NAME);
    }

    public function get_sub_category() {
        $category_id = $this->uri->segment(3);

        $sub_categories = $this->basic_model->fetch_all_records('sub_categories', ['category_id' => $category_id, 'status' => 1]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$sub_categories) {
            $data[] = ['id' => '-1', 'name' => "[Sub categories not found]"];
        } else {
            foreach ($sub_categories as $sub) {
                $data[] = ['id' => $sub->sub_category_id, 'name' => ucfirst($sub->sub_category)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function get_sub_group() {
        $group_id = $this->uri->segment(3);

        $sub_groups = $this->basic_model->fetch_all_records('sub_groups', ['group_id' => $group_id, 'status' => 1]);

        $this->output->set_content_type('application/json');
        $data = array();
        if (!$sub_groups) {
            $data[] = ['id' => '-1', 'name' => "[Sub groups not found]"];
        } else {
            foreach ($sub_groups as $sub) {
                $data[] = ['id' => $sub->sub_group_id, 'name' => ucfirst($sub->sub_group)];
            }
        }
        $this->output->_display(json_encode($data));
        return;
    }

    public function book_chapters() {
        $this->user_auth_lib->check_login();
        $data = [
            'book_chapters' => $this->basic_model->fetch_journal('book_chapter'),
        ];
        $this->user_nav_lib->run_page('journals/book_chapter/list', $data, 'Book Chapters | ' . BUSINESS_NAME);
    }

    public function add_book_chapter() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->saveEntry('book_chapter', request_post_data())) {
                notify('success', 'Book chapter saved successfully');
            } else {
                notify('error', 'Book chapter could not be saved at moment, pls try again.');
            }
            redirect(site_url('journals/book_chapters'));
        }

        $data = [
            'categories' => $this->basic_model->fetch_all_records('categories'),
            'sub_categories' => $this->basic_model->fetch_all_records('sub_categories'),
            'groups' => $this->basic_model->fetch_all_records('groups'),
            'sub_groups' => $this->basic_model->fetch_all_records('sub_groups'),
        ];

        $this->user_nav_lib->run_page('journals/book_chapter/add_book_chapter', $data, 'Add Book Chapter | ' . BUSINESS_NAME);
    }

    public function view($article, $id) {
        $id_col = '';
        $table = '';

        $this->_switch_table_col($article, $id_col, $table);

        $data = array(
            'article' => $this->basic_model->fetch_journal($table, [$id_col => $id])
        );

        $this->user_nav_lib->run_page('journals/view_article', $data, 'View Article | ' . BUSINESS_NAME);
    }
    
    private function _switch_table_col($article, &$id_col, &$table) {
        switch ($article):
            case 1:
                $id_col = 'article_id';
                $table = 'articles';
                break;
            case 2:
                $id_col = 'book_id';
                $table = 'books';
                break;
            case 3:
                $id_col = 'book_chapter_id';
                $table = 'book_chapter';
                break;
            case 4:
                $id_col = 'conference_id';
                $table = 'conference';
                break;
            case 5:
                $id_col = 'id';
                $table = 'others';
                break;
            default :
                $id_col = 'article_id';
                $table = 'articles';
        endswitch;
    }

    public function delete($article, $id) {
        $id_col = '';
        $table = '';
        
        $this->_switch_table_col($article, $id_col, $table);
        
        if ($this->basic_model->delete($table, [$id_col => $id])){
            notify('success', 'Item deleted successfully');
        }else{
            notify('error', 'Ops! your operation could not be completed at moment, pls try again');
        }
        
        redirect($this->input->server('HTTP_REFERER'));
    }
    
    public function view_index(){
        $data = array(
            'authors' => $this->basic_model->fetchAuthors()
        );
        $this->user_nav_lib->run_page('journals/index', $data, 'Index | ' . BUSINESS_NAME);
    }
    
    public function view_authors() {
        $data = array(
            'authors' => $this->basic_model->fetchAuthors()
        );
        $this->user_nav_lib->run_page('journals/authors', $data, 'Authors | ' . BUSINESS_NAME);
    }

}
