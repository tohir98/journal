<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * 
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property Basic_model $basic_model Description
 */
class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('basic_model', 'basic_model');
    }

    public function index() {
        redirect(site_url('admin/dashboard'));
//        $hdata = array(
//            'page_title' => 'Malay Journals'
//        );
//        $data = [
//            'articles' => $this->basic_model->fetch_journal('articles'),
//            'books' => $this->basic_model->fetch_journal('books'),
//            'conferences' => $this->basic_model->fetch_journal('conference'),
//            'book_chapters' => $this->basic_model->fetch_journal('book_chapter'),
//        ];
//        $this->load->view('templates/header', $hdata);
//        $this->load->view('public/journals', $data);
    }

    public function landing() {
        redirect(site_url('admin/dashboard'));
//        $hdata = array(
//            'page_title' => 'Malay Academic Database'
//        );
//
//        $this->load->view('templates/header', $hdata);
//        $this->load->view('public/landing', ['groups' => $this->basic_model->fetch_all_records('groups')]);
    }

    public function search() {
        $this->user_auth_lib->check_login();
//        $res = [];
        $res = $this->basic_model->runSearch($this->input->get('theme'));
        $hdata = array(
            'page_title' => 'Malay Academic Database'
        );

        //$this->load->view('templates/header', $hdata);

//        $this->load->view('public/landing', ['results' => $res, 'groups' => $this->basic_model->fetch_all_records('groups')]);
        $this->user_nav_lib->run_page('public/search', ['results' => $res, 'groups' => $this->basic_model->fetch_all_records('groups')]);
    }

}
