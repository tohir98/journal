<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of branch_controller
 *
 * @author TOHIR
 * 
 * @property branch_model $branch_model Description
 * @property Basic_model $basic_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 */
class Branch_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/branch_model', 'branch_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function add() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->branch_model->saveBranch(request_post_data())) {
                notify('success', 'Branch added successfully');
            } else {
                notify('error', 'Error! Unable to add branch office at moment, pls try again');
            }
            redirect(site_url('brach_office/view'));
        }
        $data = array(
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        );
        $this->user_nav_lib->run_page('branch_office/add', $data, 'New Branch | Agent CRM');
    }

    public function view() {
        $this->user_auth_lib->check_login();
        $data = array(
            'statuses' => $this->user_auth_lib->getStatuses(),
            'branches' => $this->branch_model->fetch_records()
        );

        $this->user_nav_lib->run_page('branch_office/view', $data, 'Branch Offices | Agent CRM');
    }

    public function delete($branch_id) {
        if (!isset($branch_id) || $branch_id == '') {
            trigger_error('Branch id is missing', E_USER_ERROR);
            notify('error', 'Error! Branch id is missing, pls try again');
            redirect(site_url('brach_office/view'));
        }

        if ($this->branch_model->deleteBranch($branch_id)) {
            notify('success', 'Branch Office deleted successfully');
        } else {
            notify('error', 'Error! Unable to delete branch office at moment, pls try again');
        }
        redirect(site_url('brach_office/view'));
    }

}
