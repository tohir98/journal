<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of setup_controller
 *
 * @author TOHIR
 * 
 * @property Setup_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property mailer $mailer Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Setup_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/setup_model', 's_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function categories() {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()) {
            if ($this->basic_model->save('categories', array_merge(request_post_data(), ['status' => 1]))) {
                notify('success', 'Category saved successfully');
            } else {
                notify('success', 'Unable to save category at moment, pls try again');
            }
        }

        $data = [
            'categories' => $this->s_model->fetchCategories()
        ];
        $this->user_nav_lib->run_page('setup/category_list', $data, 'Categories | ' . BUSINESS_NAME);
    }

    public function groups() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->save('groups', array_merge(request_post_data(), ['status' => 1]))) {
                notify('success', 'Group saved successfully');
            } else {
                notify('success', 'Unable to save group at moment, pls try again');
            }
        }

        $data = [
            'groups' => $this->s_model->fetchGroups()
        ];
        $this->user_nav_lib->run_page('setup/group_list', $data, 'Groups | ' . BUSINESS_NAME);
    }

    public function add_sub_group() {
        if (request_is_post()) {
            if ($this->s_model->add_sub_group(request_post_data())) {
                notify('success', 'Sub groups added successfully');
            } else {
                notify('error', 'Unable to add sub groups at moment, Pls try again');
            }

            redirect(site_url('/setup/groups'));
        }
    }
    
    public function add_sub_category() {
        if (request_is_post()) {
            if ($this->s_model->add_sub_category(request_post_data())) {
                notify('success', 'Sub categories added successfully');
            } else {
                notify('error', 'Unable to add sub categories at moment, Pls try again');
            }

            redirect(site_url('/setup/categories'));
        }
    }

   

    

    public function edit_subgroup_status($sub_group_id, $status) {
        $status_id = (int) !$status;
        if ($this->basic_model->update('sub_groups', ['status' => $status_id], ['sub_group_id' => $sub_group_id])) {
            notify('success', 'Operation successful');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/groups'));
    }
    
    public function edit_subcategory_status($sub_category_id, $status) {
        $status_id = (int) !$status;
        if ($this->basic_model->update('sub_categories', ['status' => $status_id], ['sub_category_id' => $sub_category_id])) {
            notify('success', 'Operation successful');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/categories'));
    }

    public function delete_sub_group($sub_group_id) {
        if ($this->basic_model->delete('sub_groups', ['sub_group_id' => $sub_group_id])) {
            notify('success', 'Sub group deleted successfully');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/groups'));
    }
    
    public function delete_sub_category($sub_category_id) {
        if ($this->basic_model->delete('sub_categories', ['sub_category_id' => $sub_category_id])) {
            notify('success', 'Sub category deleted successfully');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/categories'));
    }
    
    public function delete_group($group_id) {
        if ($this->basic_model->delete('groups', ['group_id' => $group_id])) {
            notify('success', 'Group deleted successfully');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/groups'));
    }
    
    public function delete_category($category_id) {
        if ($this->basic_model->delete('categories', ['category_id' => $category_id])) {
            notify('success', 'Category deleted successfully');
        } else {
            notify('error', 'Invalid parameter passed, Operation could not be completed');
        }
        redirect(site_url('/setup/categories'));
    }

}
