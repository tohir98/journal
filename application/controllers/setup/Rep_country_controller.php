<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of rep_country Controller
 *
 * @author TOHIR
 * 
 * @property Rep_country_model $rc_model Description
 * @property Basic_model $basic_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 */
class Rep_country_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/rep_country_model', 'rc_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function add() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->rc_model->saveCountry(request_post_data())) {
                notify('success', 'Representing country added successfully');
            } else {
                notify('error', 'Error! Unable to add representing country  at moment, pls try again');
            }
            redirect(site_url('rep_countries/view'));
        }
        $data = array(
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        );
        $this->user_nav_lib->run_page('rep_country/add', $data, 'Representing Counties | Agent CRM');
    }
    
    public function view(){
        $this->user_auth_lib->check_login();
        $data = [
            'countries' => $this->rc_model->fetchRepCountries()
        ];
        $this->user_nav_lib->run_page('rep_country/view', $data, 'Representing Counties | Agent CRM');
    }
    
    public function switchStatus(){
        $this->user_auth_lib->check_login();
        $data = request_post_data();

        $status = $data['state'] === 'true' ? 1 : 0;
        if ($this->rc_model->update_rep_country_status($status, $data['id'])) {
            echo 'OK';
        } else {
            echo 'error';
        }
    }

}
