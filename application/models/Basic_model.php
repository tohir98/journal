<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 */
class Basic_model extends CI_Model {

    const TBL_ARTICLE = 'articles';
    const TBL_JOURNALS = 'journals';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function fetch_journal($table, $where = []) {
        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        return $this->db->select('t.*, c.category, g.group_name, sg.sub_group, sc.sub_category')
                ->from($table . ' as t')
                ->join('categories c', 't.category_id=c.category_id', 'left')
                ->join('sub_categories sc', 't.sub_category_id=sc.sub_category_id', 'left')
                ->join('groups g', 't.group_id=g.group_id', 'left')
                ->join('sub_groups sg', 't.sub_group_id=sg.sub_group_id', 'left')
                ->get()->result();
    }
    
    public function save($table, $data) {
        $data['date_created'] = date('Y-m-d h:i:s');
            $this->db->insert($table, $data);
            
            return $this->db->insert_id();
    }

    public function saveEntry($table, $data, $chk = FALSE) {
        if (empty($data)) {
            return FALSE;
        }

        if ($chk) {
            $data['password'] = $this->user_auth_lib->encrypt($data['password']);
            $data['created_at'] = date('Y-m-d h:i:s');
            $this->db->insert($table, $data);
        } else {
            $data['date_created'] = date('Y-m-d h:i:s');
            $this->db->insert($table, $data);
            
            $j_id = $this->db->insert_id();

            $this->db->insert(self::TBL_JOURNALS, array(
                'author' => $data['author'],
                'title' => $data['title'],
                'volume' => $data['volume'],
                'issues' => $data['issues'],
                'publisher' => $data['publisher'] ? : '',
                'group_id' => $data['group_id'],
                'category_id' => $data['category_id'],
                'type' => $table,
                'url' => $data['url'],
                'year' => $data['year'],
                'keywords' => $data['keywords'],
                'abstract' => $data['abstract'],
                'pages' => $data['pages'],
                'j_id' => $j_id
            ));
        }

        return TRUE;
    }

    /*
     * @access public
     * @param int $id_bank
     * @param arr $data
     * @return success or fail
     */

    public function update($table, $data, $where) {
        $this->db->where($where);
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Class Delete anything
     * 
     * @access public
     * @return void
     */
    public function delete($table, $array) {
        $this->db->where($array);
        return $this->db->delete($table);
    }

    public function runSearch($search_text) {
        return $this->db
                ->select('j.*, g.group_name, c.category')
                ->from(self::TBL_JOURNALS . ' as j')
                ->join('groups g', 'g.group_id=j.group_id')
                ->join('categories c', 'c.category_id=j.category_id')
//                ->like('g.group_name', $search_text)
                ->like('c.category', $search_text)
                ->get()->result();
    }

    public function fetchAuthors() {
        return $this->db
                ->select('j.*, g.group_name, c.category')
                ->from(self::TBL_JOURNALS . ' as j')
                ->join('groups g', 'g.group_id=j.group_id', 'left')
                ->join('categories c', 'c.category_id=j.category_id', 'left')
                ->order_by('j.author', 'asc')
                ->get()->result();
    }

}
