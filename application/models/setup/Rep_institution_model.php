<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of branch_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 */
class Rep_institution_model extends CI_Model {

    const ACTIVE = 1;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getRepCountries() {
        return $this->db->select('rc.*, c.country')
                ->from(TBL_REP_COUNTRIES . ' as rc')
                ->join(TBL_COUNTRIES . ' as c', 'rc.country_id=c.country_id')
                ->where('rc.status', self::ACTIVE)
                ->get()->result();
    }

    public function institutionExist($institution_name) {
        $res = $this->db->get_where(TBL_REP_INSTITUTION, ['institution_name' => $institution_name])->result();
        if ($res) {
            return TRUE;
        }

        return FALSE;
    }

    public function saveRepInstitution($data, $logo) {

        if (!empty($logo) && $logo['name'] !== '') {

            $ext = end((explode(".", $logo['name'])));

            $config = array(
                'upload_path' => FILE_PATH_INSTITUTION_LOGO,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "300",
                'max_width' => "300",
            );

            $logo_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $logo_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                $error = array('error' => $this->upload->display_errors());

                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data['logo'] = $logo_name;
        }
        $this->db->insert(TBL_REP_INSTITUTION, array_merge((array) $data, ['status' => 1, 'created_at' => date('Y-m-d h:i:s'), 'created_by' => $this->user_auth_lib->get('user_id')]));

        return TRUE;
    }

    public function fetchRepresentingInstitutions($where = FALSE) {
        if ($where) {
            $this->db->where('ri.status', self::ACTIVE);
        }
        return $this->db->select('ri.*, c.country')
                ->from(TBL_REP_INSTITUTION . ' as ri')
                ->join(TBL_REP_COUNTRIES . ' as rc', 'ri.rep_country_id=rc.rep_country_id')
                ->join(TBL_COUNTRIES . ' as c', 'c.country_id=rc.country_id')
                ->get()->result();
    }

    public function toggleStatus($institution_id) {

        $set = array(
            'status' => \db\Expr::make('!status'),
            'updated_at' => \db\Expr::make('now()'),
        );


        $this->db->update(TBL_REP_INSTITUTION, $set, array(
            'institution_id' => (int) $institution_id,
        ));

        return $this->db->affected_rows() > 0;
    }

    public function updateInstitution($institution_id, $data) {
        if (empty($data)) {
            return FALSE;
        }
        return $this->db->where('institution_id', $institution_id)
                ->update(TBL_REP_INSTITUTION, $data);
    }

}
