<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH')) {
    define('BASEPATH', realpath(APP_ROOT . 'system'));
}

require APP_ROOT . 'application/config/database.php';

if (!class_exists('DBMigration')) {
    require_once 'DBMigration.php';
}

$cwd = getcwd();
chdir(__DIR__);


if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

/**
 * Your database authentication information goes here
 * @see http://dbv.vizuina.com/documentation/
 */
if (!isset($dbGroup)) {
    $dbGroup = isset($argv[2]) && trim($argv[2]) ? trim($argv[2]) : 'default';
}

if (strpos($db[$dbGroup]['hostname'], ':') !== FALSE) {
    list($dbHost, $dbPort) = explode(':', $db[$dbGroup]['hostname']);
} else {
    $dbHost = $db[$dbGroup]['hostname'];
    $dbPort = 3306;
}

$dbConfig = array(
    'host' => $dbHost,
    'port' => $dbPort ? : 3306,
    'username' => $db[$dbGroup]['username'],
    'password' => $db[$dbGroup]['password'],
    'database' => $db[$dbGroup]['database'],
);


if (isset($_SERVER['REQUEST_URI'])) {
    echo '<pre>';
    if (!$mode) {
        $mode = DBMigrationMode::NON_INTERACTIVE;
    }
}
/**
 * Implemennts DBV_Adapter_Interface
 * Stored in adapters/{DB_ADAPTER in lower case}.php
 */
if (!defined('DB_ADAPTER')) {
    define('DB_ADAPTER', 'MySQL');
}


DBMigration::el('Starting migration');

try {
    $migration = new DBMigration(__DIR__ . '/revisions/' . $dbGroup, $dbGroup);
    $migration->setDbConfig($dbConfig);
    if (!isset($startVersion)) {
        $startVersion = isset($argv[1]) && is_numeric($argv[1]) ? intval($argv[1]) : 0;
    }
    DBMigration::el('Start version : ', $startVersion);
    $migration->runMigration(isset($mode) ? $mode : DBMigrationMode::INTERACTIVE, $startVersion);
    $status = 0;
} catch (Exception $e) {
    DBMigration::el('Cannot continue, an exception occured', $e);
    $status = -1;
    //exit(-1);
}

chdir($cwd);

if (isset($_SERVER['HTTP_HOST'])) {
    echo '</pre>';
}

return $status;
