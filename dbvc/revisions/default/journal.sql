-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 29, 2016 at 09:42 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `journal`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `article_id` int(10) unsigned NOT NULL,
  `author` varchar(150) DEFAULT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `volume` varchar(150) DEFAULT NULL,
  `issues` varchar(150) DEFAULT NULL,
  `pages` int(10) unsigned DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category_id` int(10) unsigned DEFAULT NULL,
  `publisher` varchar(150) DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `keywords` longtext,
  `abstract` longtext
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`article_id`, `author`, `year`, `title`, `volume`, `issues`, `pages`, `url`, `isbn`, `category_id`, `sub_category_id`, `publisher`, `group_id`, `sub_group_id`, `date_created`, `keywords`, `abstract`) VALUES
(2, 'Azrina bint Abd Aziz', 2013, 'Introduction to academic  data mining ', '4', '23', 0, 'www.ho.com', '6543-76543', 1, 6, 'IEEE Transactions', 2, 12, '2015-11-23 07:50:53', NULL, NULL),
(3, 'Balamash, Abdullah Krunz, Marwan Nain, Philippe', 2007, 'Performance analysis of a client-side caching/prefetching system for Web traffic', '9', '4', 34, 'www.google.com', '8765-24-087', 1, 3, 'Springer', 2, 11, '2015-11-23 07:53:16', NULL, NULL),
(4, 'Basile, F. Chiacchio, P. Del Grosso, D.', 2009, 'A two-stage modelling architecture for distributed control of real-time industrial systems: Application of UML and Petri Net', '45', '96', 100, 'http://servicestatus.telstra.com/', '9045-35854-783', 1, 3, 'ACM', 1, 2, '2015-11-23 07:56:28', NULL, NULL),
(5, 'Ali Baba Dauda', 2015, 'Towards large scale Web Services performance ', '9', '20', 1, 'www.fsktm.um.edu.my', '7654-5321-800', 3, 7, 'FSKTM', 4, 15, '2015-11-23 07:58:20', NULL, NULL),
(6, 'Yves Pantai', 0, 'Chemical components of Nucleic acid', '', '65', 77, 'www.ho.com', '777-8815', 1, 6, 'Universiti Malaya', 1, 4, '2015-11-23 08:40:01', NULL, NULL),
(7, 'Bosin, Andrea Dessì, Nicoletta Pes, Barbara', 2015, 'Extending the SOA paradigm to e-Science environments', '6', '43', 92, 'www.hp.com/pdf', '91-529-638', 2, 5, '', 2, 10, '2015-11-23 08:42:54', NULL, NULL),
(8, 'Fazli Ibn  Mohammed', 2012, 'Introduction civl rights and pregogatives', '76', '4', 34, 'yuiouy.com', '6543-3-7632', 2, 4, 'FSKTM', 2, 11, '2015-12-15 08:08:19', 'War, Law, Rights', 'This Project is funded by UM Research Grant and will be parked under the Centre for Malaysian Indigenous Studies (CMIS), University of Malaya.  The information will be mapped out for public references, nationally and internationally. Researchers, academics and community will, hopefully benefit and find it practical and useful ');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) unsigned NOT NULL,
  `author` varchar(150) DEFAULT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `volume` varchar(45) DEFAULT NULL,
  `issues` varchar(45) DEFAULT NULL,
  `pages` int(10) unsigned DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group_id` int(10) unsigned DEFAULT NULL,
  `publisher` varchar(150) DEFAULT NULL,
  `editor` varchar(150) DEFAULT NULL,
  `no_of_pages` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `keywords` longtext,
  `abstract` longtext
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `author`, `year`, `title`, `volume`, `issues`, `pages`, `url`, `isbn`, `category_id`, `sub_category_id`, `group_id`, `sub_group_id`, `publisher`, `editor`, `no_of_pages`, `date_created`, `keywords`, `abstract`) VALUES
(1, 'Ammar Lanui', 2002, 'Indigenous trees of Malaysia ', '02', '04', 201, 'http://www.sabrizain.org/malaya/', '5403-3040-32-2', 1, 2, 2, 8, 'NA', 'NA', NULL, '2015-11-22 02:17:25', NULL, NULL),
(2, 'Fitri Bin Usman', 2014, 'Analysis of Oceanic metals found in Semeq beri', '75', '56', 134, 'www.none.com', '352-372', 3, 0, 2, 12, 'NA', 'NA', NULL, '2015-11-22 02:18:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_chapter`
--

CREATE TABLE `book_chapter` (
  `book_chapter_id` int(10) unsigned NOT NULL,
  `author` varchar(150) DEFAULT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `volume` varchar(45) DEFAULT NULL,
  `issues` varchar(45) DEFAULT NULL,
  `pages` int(10) unsigned DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group_id` int(10) unsigned DEFAULT NULL,
  `publisher` varchar(150) DEFAULT NULL,
  `editor` varchar(150) DEFAULT NULL,
  `book_title` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `keywords` longtext,
  `abstract` longtext
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_chapter`
--

INSERT INTO `book_chapter` (`book_chapter_id`, `author`, `year`, `title`, `volume`, `issues`, `pages`, `url`, `isbn`, `category_id`, `sub_category_id`, `group_id`, `sub_group_id`, `publisher`, `editor`, `book_title`, `date_created`, `keywords`, `abstract`) VALUES
(1, 'Ammar Mohammad Hamednaalah', 2010, 'Science and medicine of  Malay people', '5', '12', 140, 'www.anything.com', '40404-4-4-0', 1, 3, 1, 2, 'Universiti Malaya', 'Donchi', 'Malaysia 2015', '2015-11-22 02:21:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category`, `status`, `date_created`) VALUES
(1, 'Anthropology', 1, NULL),
(2, 'Education', 1, NULL),
(3, 'Health', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `conference`
--

CREATE TABLE `conference` (
  `conference_id` int(10) unsigned NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `volume` varchar(45) DEFAULT NULL,
  `issues` varchar(45) DEFAULT NULL,
  `pages` int(10) unsigned DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group_id` int(10) unsigned DEFAULT NULL,
  `publisher` varchar(150) DEFAULT NULL,
  `editor` varchar(150) DEFAULT NULL,
  `conference_name` varchar(150) DEFAULT NULL,
  `conference_location` varchar(150) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `keywords` longtext,
  `abstract` longtext
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conference`
--

INSERT INTO `conference` (`conference_id`, `author`, `year`, `title`, `volume`, `issues`, `pages`, `url`, `isbn`, `category_id`, `sub_category_id`, `group_id`, `sub_group_id`, `publisher`, `editor`, `conference_name`, `conference_location`, `date_created`, `keywords`, `abstract`) VALUES
(1, 'Ali Baba Dauda', '2015', 'Malay history and culture', '5', '12', 45, 'http://www.sabrizain.org/malaya/', '0000', 1, 6, 1, 2, 'Universiti Malaya', 'Donchi', 'Malay culture', 'Perak', '2015-11-22 02:23:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) unsigned NOT NULL,
  `group_name` varchar(45) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT '1',
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `status`, `date_created`) VALUES
(1, 'Negrito', 1, NULL),
(2, 'Senoi', 1, NULL),
(4, 'Proto Malay', 1, '2015-11-11 06:21:33');

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `id` int(10) unsigned NOT NULL,
  `author` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `volume` varchar(45) DEFAULT NULL,
  `issues` varchar(45) DEFAULT NULL,
  `publisher` varchar(150) DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(150) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `year` int(10) DEFAULT NULL,
  `keywords` longtext,
  `abstract` longtext,
  `j_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`id`, `author`, `title`, `volume`, `issues`, `publisher`, `group_id`, `category_id`, `type`, `url`, `year`, `keywords`, `abstract`, `j_id`) VALUES
(2, 'Ammar Lanui', 'Indigenous trees of Malaysia ', '02', '04', 'NA', 2, 1, 'books', 'http://www.sabrizain.org/malaya/', 2002, NULL, NULL, 1),
(3, 'Fitri Bin Usman', 'Analysis of Oceanic metals found in Semeq ber', '75', '56', 'NA', 2, 3, 'books', 'www.none.com', 2014, NULL, NULL, 2),
(4, 'Ammar Mohammad Hamednaalah', 'Science and medicine of  Malay people', '5', '12', 'Universiti Malaya', 1, 1, 'book_chapter', 'www.anything.com', 2010, NULL, NULL, 1),
(5, 'Ali Baba Dauda', 'Malay history and culture', '5', '12', 'Universiti Malaya', 1, 1, 'conference', 'http://www.sabrizain.org/malaya/', 2015, NULL, NULL, 1),
(6, 'Sara A. A. Thiam Kian, C.', 'Web Service Response Time Monitoring Architec', '7', '20', '', 4, 1, 'others', 'www.freedonchi.com', 2013, NULL, NULL, 2),
(7, 'Azrina bint Abd Aziz', 'Introduction to academic  data mining ', '4', '23', 'IEEE Transactions', 2, 1, 'articles', 'www.ho.com', 2013, NULL, NULL, 2),
(8, 'Balamash, Abdullah Krunz, Marwan Nain, Philip', 'Performance analysis of a client-side caching', '9', '4', 'Springer', 2, 1, 'articles', 'www.google.com', 2007, NULL, NULL, 3),
(9, 'Basile, F. Chiacchio, P. Del Grosso, D.', 'A two-stage modelling architecture for distri', '45', '96', 'ACM', 1, 1, 'articles', 'http://servicestatus.telstra.com/', 2009, NULL, NULL, 4),
(10, 'Ali Baba Dauda', 'Towards large scale Web Services performance ', '9', '20', 'FSKTM', 4, 3, 'articles', 'www.fsktm.um.edu.my', 2015, NULL, NULL, 5),
(11, 'Yves Pantai', 'Chemical components of Nucleic acid', '', '65', 'Universiti Malaya', 1, 1, 'articles', 'www.ho.com', 0, NULL, NULL, 6),
(12, 'Bosin, Andrea Dessì, Nicoletta Pes, Barbara', 'Extending the SOA paradigm to e-Science envir', '6', '43', '', 2, 2, 'articles', 'www.hp.com/pdf', 2015, NULL, NULL, 7),
(13, 'Fazli Ibn  Mohammed', 'Introduction civl rights and pregogatives', '76', '4', 'FSKTM', 2, 2, 'articles', 'yuiouy.com', 2012, 'War, Law, Rights', 'This Project is funded by UM Research Grant and will be parked under the Centre for Malaysian Indigenous Studies (CMIS), University of Malaya.  The information will be mapped out for public references, nationally and internationally. Researchers, academics and community will, hopefully benefit and find it practical and useful ', 8),
(14, 'Zarah Ibrahim, Umar Ameer', 'Accessing the GUI via user understanding of b', '61', '03', '', 2, 3, 'others', 'http://www.scholar.google.com/', 2015, 'GUI, Experience, HTML', 'Youth, Education and Risk: Facing the Future provides a provocative and valuable insight \r\ninto how the dramatic social and economic changes of the last twenty years have affected \r\nthe lives of Western youth. Covering young people''s attitudes towards relationships', 3);

-- --------------------------------------------------------

--
-- Table structure for table `journal_type`
--

CREATE TABLE `journal_type` (
  `journal_type_id` int(10) unsigned NOT NULL,
  `journal_type` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal_type`
--

INSERT INTO `journal_type` (`journal_type_id`, `journal_type`) VALUES
(1, 'Type A');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `requires_login`, `menu_order`) VALUES
(1, 'Users', 'users', 1, 1, 100),
(2, 'Setup', 'setup', 1, 1, 200),
(3, 'Articles', 'journals', 1, 1, 120),
(4, 'Authors', 'authors', 1, 1, 800);

-- --------------------------------------------------------

--
-- Table structure for table `module_perms`
--

CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_perms`
--

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `status`, `menu_order`) VALUES
(1, 1, 'User Mgt', 'users', 1, 1, 100),
(2, 1, 'Change Password', 'change_password', 1, 1, 100),
(3, 3, 'Journals', 'articles', 1, 1, 99),
(4, 3, 'Proceedings', 'conferences', 1, 1, 100),
(5, 2, 'Groups', 'groups', 1, 1, 100),
(6, 2, 'Categories', 'categories', 1, 1, 100),
(16, 3, 'Books', 'books', 1, 1, 105),
(17, 3, 'Book Chapters', 'book_chapters', 1, 1, 106),
(18, 3, 'Media', 'other', 1, 1, 107);

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE `others` (
  `id` int(10) unsigned NOT NULL,
  `author` varchar(150) DEFAULT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `volume` varchar(45) DEFAULT NULL,
  `issues` varchar(45) DEFAULT NULL,
  `pages` int(10) unsigned DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `keywords` longtext,
  `abstract` longtext
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `others`
--

INSERT INTO `others` (`id`, `author`, `year`, `title`, `volume`, `issues`, `pages`, `url`, `isbn`, `category_id`, `sub_category_id`, `group_id`, `sub_group_id`, `type`, `date_created`, `keywords`, `abstract`) VALUES
(2, 'Sara A. A. Thiam Kian, C.', 2013, 'Web Service Response Time Monitoring Architecture and Validation', '7', '20', 23, 'www.freedonchi.com', '098-5654-7886', 1, 2, 4, 19, 'National conference in NIlai', '2015-11-22 11:33:50', NULL, NULL),
(3, 'Zarah Ibrahim, Umar Ameer', 2015, 'Accessing the GUI via user understanding of basic HTML', '61', '03', 98, 'http://www.scholar.google.com/', '98765-8543-2345', 3, 8, 2, 10, 'Media', '2015-12-22 09:48:38', 'GUI, Experience, HTML', 'Youth, Education and Risk: Facing the Future provides a provocative and valuable insight \r\ninto how the dramatic social and economic changes of the last twenty years have affected \r\nthe lives of Western youth. Covering young people''s attitudes towards relationships');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `sub_category` varchar(45) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `category_id`, `sub_category`, `status`, `date_created`) VALUES
(2, 1, 'Human existance', 1, '0000-00-00 00:00:00'),
(3, 1, 'Social humanity', 1, '0000-00-00 00:00:00'),
(4, 2, 'Historical evidences', 1, '0000-00-00 00:00:00'),
(5, 2, 'Educational heritage', 1, '0000-00-00 00:00:00'),
(6, 1, 'Sociology', 1, '0000-00-00 00:00:00'),
(7, 3, 'Children Care', 1, '0000-00-00 00:00:00'),
(8, 3, 'Personal hygiene', 1, '0000-00-00 00:00:00'),
(9, 3, 'Early medicine', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_groups`
--

CREATE TABLE `sub_groups` (
  `sub_group_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `sub_group` varchar(150) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_groups`
--

INSERT INTO `sub_groups` (`sub_group_id`, `group_id`, `sub_group`, `status`, `date_created`) VALUES
(2, 1, 'Bateq', 1, '2015-11-11 18:26:00'),
(3, 1, 'Jahai', 1, '2015-11-11 18:26:00'),
(4, 1, 'Kensiu', 1, '2015-11-11 18:26:00'),
(5, 1, 'Kintaq', 1, '2015-11-11 18:26:00'),
(6, 1, 'Lanoh', 1, '2015-11-11 18:26:00'),
(7, 1, 'Mendriq', 1, '2015-11-11 18:26:00'),
(8, 2, 'Chewong', 1, '2015-11-11 20:15:00'),
(9, 2, 'Jah Hut', 1, '2015-11-11 20:15:00'),
(10, 2, 'Mah Meri', 1, '2015-11-11 20:15:00'),
(11, 2, 'Semai', 1, '2015-11-11 20:15:00'),
(12, 2, 'Semeg Beri', 1, '2015-11-11 20:15:00'),
(13, 2, 'Temiar', 1, '2015-11-11 20:15:00'),
(14, 4, 'Jakun', 1, '2015-11-11 22:53:00'),
(15, 4, 'Orang Kanaq', 1, '2015-11-11 22:53:00'),
(16, 4, 'Orang Kuala', 1, '2015-11-11 22:53:00'),
(17, 4, 'Orang Seletar', 1, '2015-11-11 22:53:00'),
(18, 4, 'Semelai', 1, '2015-11-11 22:53:00'),
(19, 4, 'Temuan', 1, '2015-11-11 22:53:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `user_type` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `created_at`, `last_visit`, `status`, `user_type`, `first_name`, `last_name`, `phone`) VALUES
(4, 'admin@aol.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-05 11:02:26', NULL, 1, 1, 'Super', 'Admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_perms`
--

CREATE TABLE `user_perms` (
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_perms`
--

INSERT INTO `user_perms` (`user_id`, `perm_id`, `module_id`) VALUES
(4, 1, 1),
(4, 2, 1),
(4, 3, 2),
(4, 4, 2),
(4, 5, 3),
(4, 6, 3),
(4, 7, 4),
(4, 8, 4),
(4, 9, 5),
(4, 10, 5),
(4, 11, 6),
(4, 12, 6),
(4, 13, 6),
(12, 1, 1),
(12, 2, 1),
(12, 3, 2),
(12, 4, 2),
(12, 6, 3),
(12, 8, 4),
(12, 10, 5),
(18, 1, 1),
(18, 2, 1),
(18, 3, 2),
(18, 6, 3),
(18, 8, 4),
(18, 10, 5),
(4, 14, 7),
(18, 14, 7),
(4, 15, 7),
(13, 1, 1),
(13, 2, 1),
(13, 3, 2),
(13, 4, 2),
(13, 6, 3),
(13, 8, 4),
(13, 10, 5),
(13, 14, 7),
(4, 16, 3),
(4, 17, 3),
(4, 18, 3),
(4, 19, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_chapter`
--
ALTER TABLE `book_chapter`
  ADD PRIMARY KEY (`book_chapter_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `conference`
--
ALTER TABLE `conference`
  ADD PRIMARY KEY (`conference_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_type`
--
ALTER TABLE `journal_type`
  ADD PRIMARY KEY (`journal_type_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD PRIMARY KEY (`perm_id`),
  ADD KEY `FK_module_perms_module_id` (`module_id`);

--
-- Indexes for table `others`
--
ALTER TABLE `others`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `sub_groups`
--
ALTER TABLE `sub_groups`
  ADD PRIMARY KEY (`sub_group_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_chapter`
--
ALTER TABLE `book_chapter`
  MODIFY `book_chapter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `conference`
--
ALTER TABLE `conference`
  MODIFY `conference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `journal_type`
--
ALTER TABLE `journal_type`
  MODIFY `journal_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `module_perms`
--
ALTER TABLE `module_perms`
  MODIFY `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `others`
--
ALTER TABLE `others`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sub_groups`
--
ALTER TABLE `sub_groups`
  MODIFY `sub_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD CONSTRAINT `module_perms_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
